package main

import (
	"flag"
	"os"
	"path/filepath"
)

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}

var kubeconfig *string
var incluster *bool
var publicPath *string

func init() {
	incluster = flag.Bool("incluster", false, "Turn on to get config from cluster")

	if home := homeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	publicPath = flag.String("publicPath", filepath.Join(filepath.Dir(os.Args[0]), "public/build"), "(optional) path to the public public folder")
	flag.Parse()
}
