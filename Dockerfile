FROM golang:1.14.2 as gobuilder

COPY ./*.go go.mod go.sum /go/src/gitlab.com/ajnasz/kclient/

WORKDIR /go/src/gitlab.com/ajnasz/kclient/
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o app .

FROM node:12 as reactbuilder

RUN mkdir -p /app
WORKDIR /app
COPY ./public/package.json ./public/package-lock.json /app/
RUN npm i
COPY ./public/src /app/src/
COPY ./public/public  /app/public/
RUN npm run build

FROM busybox
WORKDIR /root/
COPY --from=gobuilder /go/src/gitlab.com/ajnasz/kclient/app .
COPY --from=reactbuilder /app ./public

EXPOSE 8123
CMD ["./app"]
