package main

import (
	"context"
	"fmt"

	graphql "github.com/graph-gophers/graphql-go"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type StringMap map[string]string

func (StringMap) ImplementsGraphQLType(name string) bool { return name == "StringMap" }
func (j *StringMap) UnmarshalGraphQL(input interface{}) error {
	switch input := input.(type) {
	case StringMap:
		*j = input
		return nil
	default:
		return fmt.Errorf("wrong type")
	}
}

type objectMeta struct {
	data metav1.ObjectMeta
}

func (meta objectMeta) Name() *string {
	return &meta.data.Name
}

func (meta objectMeta) Namespace() *string {
	fmt.Println(meta.data)
	return &meta.data.Namespace
}

func (meta objectMeta) Labels() *StringMap {
	res := StringMap(meta.data.Labels)

	return &res
}

func (meta objectMeta) Annotations() *StringMap {
	res := StringMap(meta.data.Annotations)

	return &res
}

type deploymentStatus struct {
	data appsv1.DeploymentStatus
}

func (dplStatus deploymentStatus) Replicas() int32 {
	res := dplStatus.data.Replicas
	return res
}

func (dplStatus deploymentStatus) AvailableReplicas() int32 {
	res := dplStatus.data.AvailableReplicas
	return res
}

func (dplStatus deploymentStatus) ReadyReplicas() int32 {
	res := dplStatus.data.ReadyReplicas
	return res
}

func (dplStatus deploymentStatus) UnavailableReplicas() int32 {
	res := dplStatus.data.UnavailableReplicas
	return res
}

type deployment struct {
	data      *appsv1.Deployment
	k8sClient k8sClientInterface
}

func (depl deployment) ID() graphql.ID {
	return graphql.ID(depl.data.ObjectMeta.UID)
}

func (depl deployment) Name() string {
	return depl.data.ObjectMeta.Name
}

func (depl deployment) Namespace() namespace {
	ns, err := depl.k8sClient.Namespace(depl.data.ObjectMeta.Namespace)

	if err != nil {
		return namespace{}
	}
	mapper := k8sMapper{}

	return *mapper.Namespace(ns)
}

func (depl deployment) MetaData() objectMeta {
	return objectMeta{depl.data.ObjectMeta}
}

func (depl deployment) Status() *deploymentStatus {
	status := depl.data.Status
	mapper := k8sMapper{}

	return mapper.DeploymentStatus(status)
}

type namespace struct {
	data      *corev1.Namespace
	k8sClient k8sClientInterface
}

func (ns namespace) ID() graphql.ID {
	return graphql.ID(ns.data.ObjectMeta.UID)
}

func (ns namespace) Name() string {
	return ns.data.ObjectMeta.Name
}

func (ns namespace) MetaData() objectMeta {
	return objectMeta{ns.data.ObjectMeta}
}

func (ns namespace) Deployments() []*deployment {
	deployments, err := ns.k8sClient.Deployments(ns.Name())
	if err != nil {
		return nil
	}

	mapper := k8sMapper{}
	return mapper.Deployments(deployments)
}

type Resolver struct {
	k8sClient k8sClientInterface
}

func (r *Resolver) Namespaces(ctx context.Context, args struct{ ID *[]graphql.ID }) ([]*namespace, error) {
	mapper := k8sMapper{ctx, r.k8sClient}

	if args.ID != nil {
		idStrings := []string{}
		for _, id := range *args.ID {
			idStrings = append(idStrings, string(id))
		}
		namespaces, err := r.k8sClient.NamespacesByID(idStrings)
		if err != nil {
			return nil, err
		}

		return mapper.Namespaces(namespaces), nil
	}

	namespaces, err := r.k8sClient.Namespaces()
	if err != nil {
		return nil, err
	}
	return mapper.Namespaces(namespaces), nil
}

func (r *Resolver) Deployments(ctx context.Context, args struct{ Namespace string }) ([]*deployment, error) {
	deployments, err := r.k8sClient.Deployments(args.Namespace)
	if err != nil {
		return nil, err
	}

	mapper := k8sMapper{}
	return mapper.Deployments(deployments), nil

}
