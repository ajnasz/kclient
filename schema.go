package main

const graphqlSchema = `
schema {
	query: Query
}
"""
Key-value pairs of type String: String
"""
scalar StringMap
type Query {
  # returns all namespaces when IDs are not provided
  namespaces(id: [ID!]): [Namespace!]!
}
"""
Kubernetes Deployment status
"""
type DeploymentStatus {
  replicas: Int!
  readyReplicas: Int!
  availableReplicas: Int!
  unavailableReplicas: Int!
  # additional fields are optional
}
"""
Kubernetes Deployment
"""
type Deployment implements Node {
  id: ID!
  name: String!
  namespace: Namespace!
  metadata: ObjectMeta!
  status: DeploymentStatus!
  # additional fields are optional
}
type Namespace implements Node {
  id: ID!
  name: String!
  metadata: ObjectMeta!
  deployments: [Deployment!]!
  # additional fields are optional
}
"""
Kubernetes metadata
"""
type ObjectMeta {
  name: String
  namespace: String
  labels: StringMap
  annotations: StringMap
  # additional fields are optional
}
"""
An object with a unique ID
"""
interface Node {
  id: ID!
}
`
