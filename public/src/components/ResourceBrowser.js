import React from 'react';

import Namespaces from './Namespaces';
import CoverLayer from './CoverLayer';

export default function ResourceBrowser(props) {
	if (!props.namespacesData.length) return null;

	return (
		<section className="column">
			<Namespaces namespaces={props.namespacesData} refreshNamespace={props.refreshNamespace} />
			<CoverLayer isLoading={props.isLoading} />
		</section>
	);
}
