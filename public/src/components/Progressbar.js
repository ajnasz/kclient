import React from 'react';

export default function Loading(props) {
	if (!props.isLoading) return null;

	return <div className="progressbar-container">
			<progress className="progress is-small is-primary indeterminate"></progress>
		</div>
}
