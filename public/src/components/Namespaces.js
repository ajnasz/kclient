import React from 'react';

import Namespace from './Namespace';

export default function Namespaces(props) {
	const { namespaces } = props;

	if (namespaces.lenth < 1) return null;

	const list = namespaces.map((namespace) => {
		const key = 'ns-' + namespace.id;
		return <Namespace namespace={namespace} key={key} refreshNamespace={props.refreshNamespace} />;
	});
	return (
		<dl>{list}</dl>
	)
}
