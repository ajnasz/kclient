import React from 'react';
import { Link } from 'react-router-dom'

export default function NamespaceLink(props) {
	const { namespace, selectedNamespaces, className } = props;
	const isSelected = selectedNamespaces.includes(namespace.id);
	const newNamespaces = isSelected ?
		selectedNamespaces.filter(s => s !== namespace.id) :
		selectedNamespaces.concat(namespace.id);

	const uri = newNamespaces.length > 0 ? `/?namespace=${newNamespaces.sort().join(',')}` : '/';

	if (isSelected) {
		return <Link to={uri} className={className + ' is-active'} others>{namespace.name}</Link>
	}

	return <Link to={uri} className={className}>{namespace.name}</Link>
}
