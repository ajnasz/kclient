import React, { useState, useEffect } from 'react';

import Deployments from './Deployments';
import ResourceLabels from './ResourceLabels';
import RefreshButton from './RefreshButton';
import { useGraphApi, GET_NAMESPACES_BY_ID } from '../data/getData';

export default function Namespace(props) {
	const [{
		namespaces: namespacesData,
		error,
		isLoading
	}, doFetch] = useGraphApi();
	const ns = props.namespace;

	const [refresh, setRefresh ] = useState(0);

	useEffect(() => {
		if (refresh === 0) return;

		doFetch({
			query: GET_NAMESPACES_BY_ID,
			variables: { namespace: [ns.id] },
		});
	}, [refresh]);

	useEffect(() => {
		props.refreshNamespace(namespacesData);
	}, [namespacesData]);

	const dtKey = `dt-${ns.id}`;
	const ddKey = `dd-${ns.id}`;

	const output = [
		<dt key={dtKey} className="level" title={ns.id}>
			<h2 class="title is-2">{ns.name}</h2>
			<RefreshButton isLoading={isLoading} refresh={() => setRefresh((r) => r + 1)} />

		</dt>
	];
	if (error) {
		output.push(<dd key={ddKey + '-error'} className="section">
			<section className="hero is-danger">
				<div className="hero-body">
					<div className="container">
						<h1 className="title">{error.message}</h1>
					</div>
				</div>
			</section>
		</dd>);
	} else {
		output.push(<dd key={ddKey} className="content">
			<ResourceLabels labels={ns.metadata.labels} />
			<Deployments deployments={ns.deployments} namespace={ns} />
		</dd>)
	}
	return output;
}
