import React, { useEffect, useState } from 'react';
import NamespaceSelector from './NamespaceSelector';
import ResourceBrowser from './ResourceBrowser';
import { useGraphApi, GET_NAMESPACES_BY_ID } from '../data/getData';

function getNamespacesFromURL(search) {
	const searchParams = new URLSearchParams(search);

	const namespaces = searchParams.get('namespace');
	if (namespaces) {
		return namespaces.split(',');
	}

	return [];
}

export default function WebPage(props) {
	const {
		namespaces,
		setIsPageLoading,
		onError
	} = props;

	const [selectedNamespaces, setSelectedNamespaces] = useState([]);

	const [{
		namespaces: namespacesData,
		setNamespaces: setNamespacesData,
		error,
		isLoading
	}, doFetch] = useGraphApi();

	useEffect(() => {
		setSelectedNamespaces(getNamespacesFromURL(props.location.search));
	}, [props.location.search]);

	useEffect(() => {
		if (selectedNamespaces.length > 0) {
			doFetch({
				query: GET_NAMESPACES_BY_ID,
				variables: { namespaces: selectedNamespaces }
			});
		} else if (namespacesData.length > 0) {
			setNamespacesData([]);
		}
	}, [selectedNamespaces]);

	useEffect(() => {
		onError(error);
	}, [error]);

	useEffect(() => {
		setIsPageLoading(isLoading);
	}, [isLoading]);

	function refreshNamespace(newList) {
		console.log('refresh');
		setNamespacesData(namespacesData.map((ns) => {
			const newData = newList.find((l) => l.id === ns.id);
			if (newData) {
				return newData;
			}

			return ns;
		}));
	}

	if (namespacesData.length < 1) return null;

	return <div>
			<section className="columns page-content">
				<section className="column is-narrow">
					<NamespaceSelector
						selectedNamespaces={selectedNamespaces}
						namespaces={namespaces} />
				</section>

				<ResourceBrowser
					namespacesData={namespacesData}
					refreshNamespace={refreshNamespace} />
			</section>
		</div>;
}
