import React from 'react';
import ResourceLabels from './ResourceLabels';
import DeploymentStatus from './DeploymentStatus';

export default function Deployment(props) {
	const dpl = props.deployment;
	return (
		<tr>
			<td>{props.deployment.name}</td>
			<td><ResourceLabels labels={dpl.metadata.labels} /></td>
			<td><DeploymentStatus status={dpl.status} /></td>
		</tr>
	)
}
