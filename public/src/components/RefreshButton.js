import React, { useState, useEffect } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSync } from '@fortawesome/free-solid-svg-icons'

export default function RefreshButton(props) {
	const btnClassNames =[ 'button', 'is-normal'];

	if (props.isLoading) btnClassNames.push('is-loading');

	return <button type="button" className={btnClassNames.join(' ')} onClick={() => props.refresh()}>
			<span class="icon"><FontAwesomeIcon icon={faSync} /></span>
			<span>Refresh</span>
		</button>;
}
