import React from 'react';
import NamespaceSelector from './NamespaceSelector';

export default function HomeList(props){
	const key=Math.random()
	return <section className="columns page-content" key={key}>
		<section className="column is-full">
			<NamespaceSelector
				asTile={true}
				selectedNamespaces={props.selectedNamespaces}
				namespaces={props.namespaces} />
		</section>
	</section>
}
