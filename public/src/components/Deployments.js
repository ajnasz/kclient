import React from 'react';

import Deployment from './Deployment';

export default function Deployments(props) {
	const deployments = props.deployments.map((deployment) => {
		const key = 'deployment' + deployment.id;
		return <Deployment
			deployment={deployment}
			key={key}
			/>;
	});

	if (!deployments.length) return (
		<section className="hero is-primary">
		<div className="hero-body">
			<div className="container">
				<h1 className="title">No deployments found for namespace {props.namespace.name}</h1>
			</div>
		</div>
		</section>
	);
	return (
		<table className="table is-fullwidth is-hoverable">
			<caption>Deployments of {props.namespace.name}</caption>
			<thead>
				<tr>
					<th>Name</th>
					<th>Labels</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				{deployments}
			</tbody>
		</table>
	)
}
