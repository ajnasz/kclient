import React from 'react';

export default function Deployment(props) {
	const status = props.status;

	return (
		<ul>
			<li><strong>Replicas:</strong> {status.replicas}</li>
			<li><strong>Ready replicas:</strong> {status.readyReplicas}</li>
			<li><strong>Available replicas:</strong> {status.availableReplicas}</li>
			<li><strong>Unavailable replicas:</strong> {status.unavailableReplicas}</li>
		</ul>
	)
}
