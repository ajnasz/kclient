import React from 'react';

export default class PaginationItem extends React.Component {
	constructor(props) {
		super(props)
		this.classNames = ['pagination-link'];

		if (props.isCurrentPage) {
			this.classNames.push('is-current');
		}
	}
	render() {
		return (
			<li><a href="#" className={this.classNames.join(' ')} onClick={() => this.props.selectPage()}>
				{this.props.text}
			</a></li>
		)
	}
}
