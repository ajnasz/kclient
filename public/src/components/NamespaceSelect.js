import React from 'react';
import { useHistory } from 'react-router-dom'
export default function(props) {
	const history = useHistory();

	function onNamespaceCheckChange(props, event) {
		let selected = props.selectedNamespaces;
		const value = event.target.value;
		if (event.target.checked) {
			selected = selected.concat(event.target.value);
		} else {
			selected = selected.filter(s => s !== value);
		}

		const uri = selected.length > 0 ? `/?namespace=${selected.join(',')}` : '/';
		history.push(uri);
	}

	const classNames = [props.styleName];
	const className = classNames.concat([props.checked ? '' : 'is-light']).join(' ');
	return <label className="checkbox">
		<input
		type="checkbox"
		checked={props.checked}
		value={props.namespace.id}
		id={`ns-select-${props.namespace.id}`}
		name="ns-item"
		className="is-hidden"
		onChange={(event) => onNamespaceCheckChange(props, event)}
		/> <span class={className}>{props.namespace.name}</span></label>
}
