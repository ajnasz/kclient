import React from 'react';
import NamespaceLink from './NamespaceLink';

function getNamespaceStyle(namespace) {
	if (namespace.startsWith('kube-')) {
		return 'is-info';
	}
	if (namespace === 'default') {
		return 'is-warning';
	}

	return 'is-primary';
}


export default function NamespaceSelector(props) {
	function asList(props) {
		const { namespaces, selectedNamespaces } = props;

		return namespaces.map((namespace) => {
			const key = 'ns-li-' + namespace.id;
			return <li key={key} className="mb-3">
				<NamespaceLink namespace={namespace} selectedNamespaces={selectedNamespaces} />
			</li>;
		});
	}

	function asTile(props) {
		const { namespaces } = props;
		const maxItemsPerChunk = 2;
		const chunks = namespaces.reduce((out, ns) => {
			const lastIndex = out.length - 1;
			const lastChunk = out[lastIndex];
			if (lastChunk.length >= maxItemsPerChunk) {
				return out.concat([[ns]]);
			}

			out[lastIndex] = lastChunk.concat(ns);
			return out;
		}, [[]]);

		return chunks.map((namespaces, i) => {
			const key = 'ns-chunk-' + i;
			return <div className="tile is-full is-vertical" key={key}>
				{getTileChunk(props, namespaces)}
			</div>;
		});
	}

	function getTileChunk(props, namespaces) {
		const { selectedNamespaces } = props;
		return namespaces.map(namespace => {
			const key = 'ns-chunk-' + namespace.id;
			const classNames = ['tile', 'is-child', 'home-tile'];
			const className = classNames.concat(getNamespaceStyle(namespace.name)).join(' ');
			return <div className="tile is-parent" key={key}>
				<div className={className}>
					<NamespaceLink namespace={namespace} selectedNamespaces={selectedNamespaces} className="title is-3" />
				</div>
			</div>;
		});
	}


	const { namespaces } = props;
	if (namespaces.lenth < 1) return null;

	if (props.asTile) {
		return <div className="tile is-ancestor">{asTile(props, namespaces)}</div>;
	}

	return <form className="menu">
		<p className="menu-label">Namespaces</p>
		<ul className="menu-list">{asList(props)}</ul>
	</form>;
}
