import React from 'react';

import PaginationItem from './PaginationItem';

const pagePadding = 3;

export default class Pagination extends React.Component {
	generatePages(pages, currentPage) {
		let numberedPages = (new Array(pages).fill(null)).map((_, i) => {
			const page = i + 1;
			return {
				page,
				text: page,
				enabled: currentPage - pagePadding <= page && currentPage + pagePadding >= page,
			};
		}).filter(p => p.enabled);

		if (currentPage - pagePadding > 1) {
			numberedPages = [{
				page: 1,
				text: 'First',
				enabled: true
			}].concat(numberedPages);
		}

		if (currentPage + pagePadding < pages) {
			numberedPages = numberedPages.concat({
				page: pages,
				text: 'Last',
				enabled: true
			});
		}

		return numberedPages;
	}

	getRenderedPages(numberedPages, currentPage) {
		return numberedPages.map(({text, page}) => {
			const isCurrentPage = currentPage === page;
			const key = "pagination-page" + page + isCurrentPage;
			return <PaginationItem
				text={text}
				isCurrentPage={isCurrentPage}
				key={key}
				selectPage={() => this.props.selectPage(page)} />
		});
	}

	render() {
		const pages = Math.max(1, Number.parseInt(this.props.pagination.pages, 10) || 1);

		if (pages < 2) return null;
		const currentPage = Math.max(1, Number.parseInt(this.props.pagination.currentPage, 10) || 1);
		const numberedPages = this.generatePages(pages, currentPage);
		const renderedPages = this.getRenderedPages(numberedPages, currentPage);

		return (
			<div className="pagination is-centered">
				<ul className="pagination-list">{renderedPages}</ul>
			</div>
		);
	}
}
