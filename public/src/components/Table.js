import React from 'react';

import TableRow from './TableRow';

export default class Table extends React.Component {
	render() {
		const rows = this.props.rows.map((row) => {
			const key = 'table-row' + row.id;
			return <TableRow
				rowData={row}
				key={key} selectRow={() => this.props.selectRow(row.id)}
				isSelected={this.props.selectedRow === row.id} />;
		});
		return (
			<table className="table is-fullwidth is-striped is-hoverable">
				<thead>
				<tr>
					<th>Name</th>
					<th>Phase</th>
					<th>IP</th>
				</tr>
				</thead>
				<tbody>
					{rows}
				</tbody>
			</table>
		)
	}
}
