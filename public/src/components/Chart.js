import React from 'react';
import {
	Legend,
	Line,
	LineChart,
	Tooltip,
	XAxis,
	YAxis,
} from 'recharts';

function mapData(row) {
	if (!row || !row.data) {
		return [];
	}

	return row.data.map(item => ({ foo: item }))
}

export default class Chart extends React.Component {
	render() {
		const data = mapData(this.props.selectedRow);
		return (<div>
			<h2 className="title is-2 is-spaced">{this.props.selectedRow.name}</h2>
			<LineChart width={700} height={300} data={data}>
				<Line type="monotone" dataKey="foo" stroke="#8884d8" />
				<Legend />
				<Tooltip />
				<XAxis />
				<YAxis />
			</LineChart>
		</div>);
	}
}
