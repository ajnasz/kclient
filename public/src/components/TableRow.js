import React from 'react';

export default class TableRow extends React.Component {
	render() {
		const row = this.props.rowData;
		let classNames = 'table-row';

		if (this.props.isSelected) {
			classNames += ' is-selected';
		}
		return (
			<tr className={classNames} onClick={() => this.props.selectRow()}>
				<td>{row.name}</td>
				<td>{row.status.phase}</td>
				<td>{row.status.podIP}</td>
			</tr>
		);
	}
}
