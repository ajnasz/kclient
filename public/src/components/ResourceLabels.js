import React from 'react';

export default function ResourceLabels(props) {
	if (!props.labels) return null;
	const labels = Object.entries(props.labels);
	if (labels.length < 1) return null;

	const list = labels.map(([name, value]) => {
		const key = `k-${name}-${value}`;
		return <li key={key}><strong>{name}:</strong> {value}</li>
	});

	return <ul>{list}</ul>;
}
