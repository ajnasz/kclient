import React from 'react';

import HomeList from './HomeList';

function getNamespacesFromURL(search) {
	const searchParams = new URLSearchParams(search);

	const namespaces = searchParams.get('namespace');
	if (namespaces) {
		return namespaces.split(',');
	}

	return [];
}
export default function HomePage(props) {
	const selectedNamespaces = getNamespacesFromURL(props.location.search);

	if (selectedNamespaces.length) return null;

	if (props.namespaces.length < 1) {
		return <div class="notification is-info is-light">
			<h2 class="title">No namespaces found</h2>
		</div>;
	}

	return <div>
		<HomeList
			namespaces={props.namespaces}
			selectedNamespaces={props.selectedNamespaces} />
	</div>
}
