const API_SERVER_URL='http://localhost:10000'

export default async function getPods() {
	const url = new URL(API_SERVER_URL);
	url.pathname = '/pods';
	const res = await fetch(url.href)
	const json = await res.json();
	return json;
}
