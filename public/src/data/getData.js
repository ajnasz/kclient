import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import { useState, useEffect } from 'react';


const API_URI = process.env.REACT_APP_API_URI ? process.env.REACT_APP_API_URI : '/query' ;
const client = new ApolloClient({ uri: API_URI });

export const GET_ALL_NAMESPACES = gql`{
		namespaces{
			name,
			id,
		},
}`;

export const GET_NAMESPACES_BY_ID = gql`query Query($namespaces: [ID!]){
		namespaces(id: $namespaces){
			name,
			id,
			metadata{
				labels,
				annotations,
			}
			deployments{
				id,
				name,
				metadata{
					labels,
					annotations,
				},
				status{
					replicas,
					readyReplicas,
					availableReplicas,
					unavailableReplicas,
				},
			},
		},
}`;

export function useGraphApi() {
	const [namespaces, setNamespaces] = useState([]);
	const [query, setQuery] = useState(null);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(null);

	useEffect(() => {
		async function fetchData() {
			if (!query) return;
			setIsLoading(true);
			setError(null);
			try {
				const res = await client.query({
					...query,
					fetchPolicy: 'no-cache',
				});
				// await new Promise(r => setTimeout(r, 600));
				setNamespaces(res.data.namespaces);
			} catch (err) {
				setError(err);
				setNamespaces([]);
			}
			setIsLoading(false);
		}

		fetchData();
	}, [query]);

	return [{namespaces, setNamespaces, error, isLoading}, setQuery];
}
