import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import Progressbar from './components/Progressbar';
import WebPage from './components/WebPage';
import HomePage from './components/HomePage';

import { useGraphApi, GET_ALL_NAMESPACES } from './data/getData';
import './index.css';

function App() {
	function sortNamespaces({name: nsa}, {name: nsb}) {
		if (nsa === nsb) return 0;
		if (nsa === 'default') return -1;
		if (nsb === 'default') return 1;
		if (nsa.startsWith('kube-') && nsb.startsWith('kube-')) {
			return nsa > nsb ? 1 : -1;
		}
		if (nsa.startsWith('kube-')) {
			return 1;
		}
		if (nsb.startsWith('kube-')) {
			return -1;
		}

		return nsa > nsb ? -1 : 1;
	}

	const [{namespaces, error, isLoading}, doFetch] = useGraphApi();
	const [isPageLoading, setIsPageLoading] = useState(false);
	// const [namespaces, setNamespaces] = useState([]);
	const [selectedNamespaces, setSelectedNamespaces] = useState([]);
	const [errorMsg, setErrorMsg] = useState('');

	function onNamespaceChange(namespaces) {
		setSelectedNamespaces(namespaces.sort(sortNamespaces));
	}

	function onError(err) {
		if (err) {
			const msg = 'Error fetching data: ' + err.message;
			setErrorMsg(msg + err.stack);
		} else {
			setErrorMsg('');
		}
	}

	useEffect(() => {
		doFetch({query: GET_ALL_NAMESPACES})
	}, []);

	useEffect(() => {
		onError(error);
	}, [error]);

	useEffect(() => {
		setIsPageLoading(isLoading);
	}, [isLoading]);

	if (errorMsg) {
		return <main>
			<section className="section">
			<section className="hero is-danger">
				<div className="hero-body">
					<div className="container">
						<h1 className="title">{errorMsg}</h1>
					</div>
				</div>
				</section>
				</section>
			</main>
	}

	return <main>
		<section className="section">
			<header className="page-title">
				<h1 className="title is-1 is-spaced">KClient</h1>
			</header>
			<Router>
				<Route exact path="/" render={(props) => <HomePage {...props}
					namespaces={namespaces.sort(sortNamespaces)}
					selectedNamespaces={selectedNamespaces}
					onNamespaceChange={onNamespaceChange}
					/>}
				/>

				<Route path="/" render={(props) => <WebPage {...props}
					namespaces={namespaces.sort(sortNamespaces)}
					onError={onError}
					selectedNamespaces={selectedNamespaces}
					onNamespaceChange={onNamespaceChange}
					setIsPageLoading={setIsPageLoading}
					/>}
				/>
			</Router>
			<Progressbar isLoading={isPageLoading} />
		</section>
	</main>;
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
