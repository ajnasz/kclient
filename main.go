package main

import (
	"log"
	"net/http"
	"os"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/rs/cors"
)

var k8sClient *K8SClient

func startServer(k8sClient k8sClientInterface) {
	opts := []graphql.SchemaOpt{graphql.UseFieldResolvers(), graphql.MaxParallelism(20)}
	schema := graphql.MustParseSchema(graphqlSchema, &Resolver{k8sClient}, opts...)

	http.Handle("/query", cors.Default().Handler(&relay.Handler{Schema: schema}))
	http.Handle("/", http.FileServer(http.Dir(*publicPath)))
	port := os.Getenv("PORT")

	if port == "" {
		port = "10000"
	}

	log.Printf("Listening on %s\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func main() {
	k8sClient = NewK8SClient()

	startServer(k8sClient)
}
