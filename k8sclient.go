package main

import (
	"fmt"
	"os"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	// metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeinformers "k8s.io/client-go/informers"
	appsv1lister "k8s.io/client-go/listers/apps/v1"
	corev1lister "k8s.io/client-go/listers/core/v1"

	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	// "k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
)

type deploymenCache struct {
	Deloyments []*appsv1.Deployment
}

func newNamespaceLister(clientset *kubernetes.Clientset) corev1lister.NamespaceLister {
	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(clientset, time.Second*30)
	nsLister := kubeInformerFactory.Core().V1().Namespaces().Lister()
	stop := make(chan struct{})
	kubeInformerFactory.Start(stop)

	return nsLister
}

func newDeploymentLister(clientset *kubernetes.Clientset) appsv1lister.DeploymentLister {
	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(clientset, time.Second*30)
	dplLister := kubeInformerFactory.Apps().V1().Deployments().Lister()
	stop := make(chan struct{})
	kubeInformerFactory.Start(stop)

	return dplLister
}

func NewK8SClient() *K8SClient {
	var err error
	var config *rest.Config
	if *incluster {
		config, err = rest.InClusterConfig()
	} else {
		config, err = clientcmd.BuildConfigFromFlags("", *kubeconfig)
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v", err)
		os.Exit(1)
		return nil
	}

	clientSet, err := kubernetes.NewForConfig(config)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v", err)
		os.Exit(1)
		return nil
	}

	k8sClient = &K8SClient{
		clientSet,
		newDeploymentLister(clientSet),
		newNamespaceLister(clientSet),
	}

	return k8sClient
}

type k8sClientInterface interface {
	Namespaces() ([]*corev1.Namespace, error)
	NamespacesByID([]string) ([]*corev1.Namespace, error)
	Namespace(ns string) (*corev1.Namespace, error)
	Deployments(ns string) ([]*appsv1.Deployment, error)
	Deployment(ns string, dpl string) (*appsv1.Deployment, error)
}

// K8SClient implements k8sClientInterface
type K8SClient struct {
	Clientset        *kubernetes.Clientset
	DeploymentLister appsv1lister.DeploymentLister
	NamespaceLister  corev1lister.NamespaceLister
}

// Namespaces returns all namespaces
func (k8s *K8SClient) Namespaces() ([]*corev1.Namespace, error) {
	return k8s.NamespaceLister.List(labels.Everything())
}

// Namespace returns all namespaces
func (k8s *K8SClient) Namespace(name string) (*corev1.Namespace, error) {
	return k8s.NamespaceLister.Get(name)
}

// NamespacesByID returns namespaces filtered by ID
func (k8s *K8SClient) NamespacesByID(IDs []string) ([]*corev1.Namespace, error) {
	namespaces, err := k8s.Namespaces()

	if err != nil {
		return nil, err
	}

	out := []*corev1.Namespace{}

	for _, ID := range IDs {
		for _, ns := range namespaces {
			if string(ns.ObjectMeta.UID) == ID {
				out = append(out, ns)
			}
		}
	}

	return out, nil
}

// Deployments returns deployments of a namespace
func (k8s *K8SClient) Deployments(namespace string) ([]*appsv1.Deployment, error) {
	return k8s.DeploymentLister.Deployments(namespace).List(labels.Everything())
}

// Deployment returns a deployment with the given name from a namespace
func (k8s *K8SClient) Deployment(namespace, name string) (*appsv1.Deployment, error) {
	return k8s.DeploymentLister.Deployments(namespace).Get(name)
}
