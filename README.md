# KClient

## Clone the repository

```
git clone https://gitlab.com/ajnasz/kclient.git $HOME/src/kclient
cd $HOME/src/kclient
```

## Build api server

Tested on

```sh
$ go version
go version go1.14.2 linux/amd64
```

```sh
go build .
```

## Build frontend

Tested with nodejs 12

```sh
$ cd public
$ npm install
$ npm run build
```

In case you're planning to serve the API from a different host you'll need to set the `REACT_APP_API_URI` env var to it's host during build:

```sh
$ REACT_APP_API_URI="http://foo.bar.com/query" npm run build
```

## Start the application locally

It's expected that you have configured access to a kubernetes cluster.

```sh
./kclient
```

The application will load the kubeconfig file from `$HOME/.kube/config` by default, it can be overridden with the `-kubeconfig` flag:

```sh
./kclient -kubeconfig /path/to/kubernetes/config
```

The application will listen on port ':10000', to override it use `PORT` environment variable.

```sh
PORT=2211 ./kclient -kubeconfig /path/to/kubernetes/config
```

The application can serve the client application as well, by default it's expected to be in the `public/dist` folder, but it can be overridden with the `-publicPath` flag.

```sh
PORT=2211 ./kclient -kubeconfig /path/to/kubernetes/config -publicPath /path/to/public/dist
```

## Start the application in a kubernetes cluster

If you set the `-incluster` flag, the application will expect that it's running in a kubernetes cluster and it will use service account token mounted inside the POD.

For a kubernetes configuration example check example/kclient.yml
To build the docker image check the Dockerfile
