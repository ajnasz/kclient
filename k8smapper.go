package main

import (
	"context"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
)

type k8sMapper struct {
	ctx       context.Context
	k8sClient k8sClientInterface
}

func (mapper k8sMapper) Namespace(ns *corev1.Namespace) *namespace {
	return &namespace{ns, mapper.k8sClient}
}

func (mapper k8sMapper) Namespaces(namespaces []*corev1.Namespace) []*namespace {
	out := []*namespace{}
	for _, ns := range namespaces {
		out = append(out, mapper.Namespace(ns))
	}
	return out
}

func (mapper k8sMapper) Deployment(depl *appsv1.Deployment) *deployment {
	return &deployment{depl, mapper.k8sClient}
}

func (mapper k8sMapper) Deployments(deployments []*appsv1.Deployment) []*deployment {
	out := []*deployment{}
	for _, dpl := range deployments {
		out = append(out, mapper.Deployment(dpl))
	}

	return out
}

func (_ k8sMapper) DeploymentStatus(dplStatus appsv1.DeploymentStatus) *deploymentStatus {
	return &deploymentStatus{dplStatus}
}
